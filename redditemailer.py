#!/usr/bin/python
import praw, os, re, time, smtplib
from email.mime.text import MIMEText
from config import *

server = smtplib.SMTP(smtpserver,smtpport)
server.login(smtpusername,smtppassword)

done_this_time = set()

if not os.path.isfile(doneIDsFile):
	open(doneIDsFile, 'a').close()

with open(doneIDsFile) as doneids:
	done_already = doneids.read().splitlines()

r = praw.Reddit(user_agent="Reddit Emailer, Watches selected subreddits, emails a list of users when there is a post. By /u/chpwssn")
for subreddit in subreddits:
	submissions = r.get_subreddit(subreddit).get_new(limit=getcount)
	for submission in submissions:
		if submission.id not in done_this_time and submission.id not in done_already:
			done_this_time.add(submission.id)
			sub = vars(submission)
			print sub['title']
			msg = MIMEText("New post in "+subreddit+", see it at "+sub['permalink']+".")
			msg['Subject'] = "New post in "+subreddit
			msg['From'] = smtpusername
			for recipient in subscribers:
				msg['To'] = recipient
				server.sendmail(smtpusername,recipient,msg.as_string())
			with open(doneIDsFile, "a") as doneids:
				doneids.write(submission.id+'\n')
server.quit()