# README #

This bot watches a list of subreddits for new posts and emails the subscribers list when it sees one it hasn't seen before. It is quick and dirty.


### How do I get set up? ###

* Make sure you have PRAW (the other dependencies should already be in Python)
* git clone
* copy example-config.py to config.py
* Edit your config.py
* python redditemailer.py


Questions? Find me on reddit /u/chpwssn