#the subreddits you want to watch
subreddits=["opendirectories","pics"]

#the people you want to email
subscribers=["person1@example.com","person2@example.com"]

#the file to keep track of posts
doneIDsFile="./downloadedids.txt"

#how many to get in one run
getcount=25

#SMTP server info
smtpserver="example.com"
smtpport="25"
smtpusername="bot@example.com"
smtppassword="password"